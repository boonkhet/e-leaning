<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>E-learning</title>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
</head>

<body class="bg">
    <div class="container-fluid">
      <h2 style="color:#fff; padding-top:20px;">รายการวีดีโอ <small><a style="color:green;" href="./addcontant.php">[+add]</a></small> </h2>
      <form style="background:#fff; padding-left:30px;padding-top:5px; border-radius:5px;" action="./upload.php" method="post" enctype="multipart/form-data" >
        <input type="file" name="fileToUpload" id="fileToUpload">
        <input class="btn " type="submit" value="Upload Contant  " name="submit">
      </form>
   
    <table class="table table-striped table-dark" style="padding-top:20px; margin-top:20px;">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Detail</th>
      <th scope="col">Link</th>
      <th scope="col">Active</th>
    </tr>
  </thead>
  <tbody>

    <?php
        require '../php/open_connect.php';
        $sql = "SELECT * FROM tblVideo";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {   
    ?>

    <tr>
      <th scope="row"><?php echo $row["id"];?> <a style="color:red;" href="./del_video.php?id=<?php echo $row["id"];?>">[-]</a></th>
      <td><?php echo $row["title"];?></td>
      <td><?php echo $row["detail"];?></td>
      <td><?php echo $row["path"];?></td>

      <?php 
        if($row["isShow"]=='y'){
          echo'<td><a href="./active_con.php?id='.$row["id"].'&status=n"><button type="button" class="btn btn-success">Actice</button></a></td>';
        }else{
            echo'<td><a href="./active_con.php?id='.$row["id"].'&status=y"><button type="button" class="btn btn-danger">Inactive</button></a></td>';
        }
      ?>
    </tr>

    <?php 
        }
        }else{
            
        }
        require '../php/close_connect.php';
    ?>
    
  </tbody>
</table>
    </div>
</body>
</html>



