<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>E-learning</title>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <link rel="stylesheet" type="text/css" href="./css/login.css">

</head>

<?php 
function generateRandomString($length) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz'; //ABCDEFGHIJKLMNOPQRSTUVWXYZ
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
  }
?>

<body class="bg">
    <div class="container-fluid">
        <div class="row">
            <a href="./contant.php" class="textback"> < กลับหน้าหลัก</a>
        </div>
     </div>
    <div class="vid-container">
    <div class="inner-container">
      <div class="box">
        <h1>เพิ่มวีดีโอ</h1>
        <form action="./add_con.php" method="post">
          <input type="text" name="name"  placeholder="ชื่อวีดีโอ"/>
          <input type="text" name="detail" placeholder="รายละเอียดวีดีโอ"/>
          <select class="input-dropdown"name="link">
            <?php 
              foreach(glob('../assets/video/*.*') as $filename){
                echo '<option value="'.str_replace("../assets/","",$filename).'">'.str_replace("../assets/video/","",$filename).'</option>';
              }
            ?>
          </select>

          <button type="submit">Save</button>
        </form>
      </div>
    </div>
  </div>

</body>
</html>



