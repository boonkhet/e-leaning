<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-LEARNING</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- <link rel="stylesheet" type="text/css" href="./css/style.css"> -->
<link rel="stylesheet" type="text/css" href="./css/list.css">

<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">
<body class="bg">

<div class="container-fluid">

   <div class="row">
    <h1 class="title-top">วีดีโอทั้งหมดในรายการ</h1>

    <div class="userbar">
       <a href="./profile.php"><h5 class="textuser"><img class="imguser" src="./assets/image/user.png" alt="">&nbsp;<?php echo $_COOKIE['username']; ?></h5> 
       </a> 
       
    </div>
   </div>
  <div class="row">

    <?php
        require './php/open_connect.php';
        $sql = "SELECT * FROM tblVideo WHERE isShow = 'y' order by seq ";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {   
    ?>
    <div class="col-12 col-md-4">
        <div class="card">
        <img class="card-img-top" src="./assets/<?php echo $row["imgCover"];?>" alt="">
            <div class="card-body">
                <h5 class="card-title"><?php echo $row["title"]; ?></h5>
                <p class="card-text"><?php echo $row["detail"]; ?></p>
                <a href="./main.php?id=<?php echo $row["id"];?>" class="btn btn-primary">ดูวีดีโอ</a>
            </div>
        </div>
    </div>
    <?php 
        }
        }else{
            
        }
        require './php/close_connect.php';
    ?>


  </div>
</div>
</body>

</html>