<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-LEARNING</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">
<body>

    <?php 
        $id = $_GET['id'];
    ?>

    <div class="container-fluid">
            <div class="row" style="background:#000!important;">
                <div class="col-md-9 col-12 no-space" >   
                        <?php
                            require './php/open_connect.php';
                            $sql = "SELECT * FROM tblVideo WHERE id = '$id' ";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {   
                        ?>
                <div>
                        <a href="./list.php" class="textback"> < กลับสู่หน้าบทเรียน</a>
                    </div>
                    <video class="video" controls autoplay muted><source src="./assets/<?php echo $row["path"];?>" type="video/mp4"></video>
                </div>
                <div class="col-md-3 col-12 no-space bg-bar" >
                        <!-- <img class="bar" src="./assets/image/bg1.jpeg">  -->
                        <div style="overflow: scroll!important; height:100vh;" >
                        <h4 style="color:#fff; width:90%; margin-left:10%; margin-top:25px;">วีดีโอที่กำลังเล่น</h4>
                        
                        <div class="col-12 col-md-12">
                            <div class="card" style="width:90%; margin-left:5%; margin-top:15px; margin-bottom:15px;">
                            <img class="card-img-top" src="./assets/<?php echo $row["imgCover"];?>" alt="">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $row["title"]; ?></h5>
                                    <p class="card-text"><?php echo $row["detail"]; ?></p>
                                    <a href="./main.php?id=<?php echo $row["id"];?>" class="btn btn-primary">ดูวีดีโอ</a>
                                </div>
                            </div>
                        </div>

                        <?php 
                            }
                            }else{
                                
                            }
                            require './php/close_connect.php';
                        ?>

                        <h4 style="color:#fff; width:90%; margin-left:10%; margin-top:25px;">วีดีโออื่นๆ</h4>

                        <?php
                            require './php/open_connect.php';
                            $sql = "SELECT * FROM tblVideo WHERE isShow = 'y' AND id != '$id' order by seq ";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {   
                        ?>
                        <div class="col-12 col-md-12">
                            <div class="card" style="width:90%; margin-left:5%; margin-bottom:15px;">
                            <img class="card-img-top" src="./assets/<?php echo $row["imgCover"];?>" alt="">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $row["title"]; ?></h5>
                                    <p class="card-text"><?php echo $row["detail"]; ?></p>
                                    <a href="./main.php?id=<?php echo $row["id"];?>" class="btn btn-primary">ดูวีดีโอ</a>
                                </div>
                            </div>
                        </div>

                        <?php 
                            }
                            }else{
                                
                            }
                            require './php/close_connect.php';
                        ?>
                        </div>

                </div>
            </div>
    </div>
</body>
</html>